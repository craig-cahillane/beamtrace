"""
Example 1
Scan a simple seeded beam scan through two flat dielectric surfaces,

          |        |
--- n1 -->|   n2   |--- n1 -->
          |        |

Craig Cahillane
July 14, 2022
"""

# LIGO arm cavity
import numpy as np
from beamtrace.tracer import BeamTrace

q_input = -1.0 + 0.1j
length_1 = 0.5  # m
length_sub = 0.5 # m
length_2 = 0.5 # m

roc = np.inf
n1 = 1.0
nn = 2.0

beam = BeamTrace(is_cavity=False)

beam.set_seed_beam(q_input)

beam.add_space(length_1, name="input length")
beam.add_lens(roc, name="front surface")
beam.add_space(length_sub, index_of_refraction=nn, name="substrate length")
beam.add_lens(roc, name="back surface")
beam.add_space(length_2, name="output length")

beam.print()

# Make an equivalent beam trace without using lens or index of refraction in the space
# Here, the refractions are also just identity matrices, same as the lenses above.
beam2 = BeamTrace(is_cavity=False)

beam2.set_seed_beam(q_input)

beam2.add_space(length_1, name="input length")
beam2.add_refraction(roc, n1, nn, name="front surface")
beam2.add_space(length_sub, index_of_refraction=nn, name="substrate length")
beam2.add_refraction(roc, nn, n1, name="back surface")
beam2.add_space(length_2, name="output length")

beam2.print()

import matplotlib.pyplot as plt

fig = beam.plot_beam_scan()
beam2.plot_beam_scan(color="C4", ls="--", label="Same beam using refractions", fig=fig)
axes = fig.get_axes()
axes[0].set_title(f"Beam trace through flat dielectric with index of refraction nn = {nn}")
plt.show()
