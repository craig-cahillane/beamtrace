"""
Example 1
Scan a simple seeded beam scan through one flat and one curved dielectric surface

          |       \
--- n1 -->|   n2   |--- n1 -->
          |       /

Craig Cahillane
July 14, 2022
"""

# LIGO arm cavity
import numpy as np
from beamtrace.tracer import BeamTrace

q_input = -1.0 + 1.0j
length_1 = 0.5  # m
length_sub = 0.5 # m
length_2 = 0.5 # m

# R > 0 for concave surface using refraction 
# c.f. Table 15.1 (e) of Siegman, "Lasers"
roc = 0.5 # m
n1 = 1.0
nn = 2.0

# Second refraction is at a curved surface of a dielectric
beam = BeamTrace(is_cavity=False)

beam.set_seed_beam(q_input)

beam.add_space(length_1, name="input length")
beam.add_refraction(np.inf, n1, nn, name="flat front surface")
beam.add_space(length_sub, index_of_refraction=nn, name="substrate length")
beam.add_refraction(roc, nn, n1, name="back surface")
beam.add_space(length_2, name="output length")

beam.print()

import matplotlib.pyplot as plt

fig = beam.plot_beam_scan()
axes = fig.get_axes()
axes[0].set_title(f"Beam trace through curved dielectric with curvature = {roc:.1f} m and index of refraction nn = {nn}")
plt.show()
