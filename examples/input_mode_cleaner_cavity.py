"""
Create a model of the IMC triangular cavity path using the system parameters noted in D0902838-v5

R3 (MC3) = inf m
 ____                             
|    /        L2 = 16.241 m
| 3 / ------------
|__/  -           ------------
      -    	              ------------	     	  	          ____
      -    	     	                  ------------   	     \    |
      - L3 = 0.4649 m  	     	      			------------  | 2 |  R2 (MC2) = 27.275 m
      -            	                  ------------   	     /____|
      -                       ------------
 __   -           ------------
|  \  ------------    L1 = 16.241 m
| 1 \
|____\

R1 (MC1) = inf m


"""

import beamtrace
import matplotlib.pyplot as plt

# LIGO arm cavity
import numpy as np
from beamtrace.tracer import BeamTrace

# mirror radii of curviture RoC
R1 = np.inf  # m
R2 = 27.275  # m
R3 = np.inf  # m

# distances between mirrors
L1 = 16.24056  # m
L2 = 16.24060
L3 = 0.4649  # m

# initialize class
IMCt = BeamTrace(is_single_pass=True)
# round path sequence
IMCt.add_mirror(R1, name="MC1")
IMCt.add_space(L1)
IMCt.add_mirror(R2, name="MC2")
IMCt.add_space(L2)
IMCt.add_mirror(R3, name="MC3")
# complete the triangle
IMCt.add_space(L3)


# calc and print beam paramters and mechanical sequence effects
IMCt.calculate_cavity_abcd()  # cavity round-trip ABCD matrix, stability, stab-cond. [class].q_input parameter

# zz, ww, gouy, qq = IMCt.scan_cavity(round_trip=False) #prop distance, beam radius, gouy phase, q-parameter
print("\n Total Round-trip ABCD = \n{}\n".format(IMCt.abcd))
print("Eigenmode q-parameter = {} \n".format(IMCt.q_input))
print(
    "Path Accumulated Gouy Phase = {:.2f} deg \n".format(
        IMCt.get_total_cavity_gouy_phase() / 2.0
    )
)
print(
    "There and Back Accumulated Gouy Phase = {:.2f} deg \n".format(
        IMCt.get_total_cavity_gouy_phase()
    )
)

# graphs
fig = IMCt.plot_cavity_scan()
plt.show()
