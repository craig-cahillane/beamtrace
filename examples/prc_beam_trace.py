"""
Example 4
PRC cavity at LHO
Create a simple two mirror cavity with radius of curvature of 1.0 meter,
and distance between the mirrors of 0.5 meters
prm R = -5.69 m                   pr2 R = -6.4 m
 ___                             ___
|   \         L1 = 15.75        /   |
|    | ----------------------- |    |
|___/                   -----   \___|
        L2 = 0.5 m -----
 ____         -----                                ____              ____
|   /    -----         L3 = 19.36 m               /   /  L4 ~ 5.0 m |   /
|  |---------------------------------------------/---/--------------|--|
|___\                                           /___/               |___\
R3 = 35.972                                   BS R = inf          itmy R = -1934
"""
import os

import beamtrace
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from beamtrace.tracer import BeamTrace

mpl.rcParams.update(
    {
        "figure.figsize": (12, 9),
        "text.usetex": True,
        "font.family": "serif",
        # 'font.serif': 'Georgia',
        # 'mathtext.fontset': 'cm',
        "lines.linewidth": 2.5,
        "font.size": 16,
        "xtick.labelsize": "large",
        "ytick.labelsize": "large",
        "legend.fancybox": True,
        "legend.fontsize": 18,
        "legend.framealpha": 0.9,
        "legend.handletextpad": 0.5,
        "legend.labelspacing": 0.2,
        "legend.loc": "best",
        "legend.columnspacing": 2,
        "savefig.dpi": 80,
        "pdf.compression": 9,
    }
)

# Set up fig_dir
script_dirname = os.path.dirname(os.path.abspath(__file__))
# this returns "powers_from_meas"
script_name = os.path.basename(os.path.abspath(__file__)).rstrip(".py")

fig_dir = os.path.abspath(os.path.join(script_dirname, "..", f"figures/{script_name}"))
print("Figures made by this script will be placed in:")
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

### PRC parameters
nn = 1.44963  # index of refraction of silica

roc_prm = -10.948  # m vs -5.7150 m = 0.0212 m Finesse - Fulda
roc_pr2 = -4.543  # m vs -6.4240 m = 0.0180 m Finesse - Fulda
roc_pr3 = 36.021  # m vs 36.0130 m = -0.0402 m Finesse - Fulda
roc_bs = np.inf
roc_itmy = -1934.0000  # m vs -1939.3900 m = 5.3900 m Finesse - Fulda

ll_prm_pr2 = 16.6128  # m vs 15.7400 m = 0.0186 m Finesse - Fulda
ll_pr2_pr3 = 16.1626  # m vs 15.4601 m = -0.0166 m Finesse - Fulda
ll_pr3_bsar = 19.5381  # m vs 19.3661 m = 0.0000 m Finesse - Fulda
ll_bs_sub = 0.0687  # m vs 0.0685 m = 0.0002 m Finesse - Fulda
ll_bs_itmy_ar = 5.0126  # m vs 4.9670 m = 0.0456 m Finesse - Fulda
ll_itmysub = 0.2000  # m vs 0.2000 m = 0.0000 m Finesse - Fulda

total_length_prc = 57.656  # m vs 55.8017 m = 0.0478 m Finesse - Fulda

f_itmy = 34500.0000  # m vs 34500.0000 m = 0.0000 m Finesse - Fulda

### Set up BeamTrace
prc_abcd = BeamTrace()

prc_abcd.add_mirror(roc_prm, name="prm")
prc_abcd.add_space(ll_prm_pr2, name="L prm to pr2")
prc_abcd.add_mirror(roc_pr2, name="pr2")
prc_abcd.add_space(ll_pr2_pr3, name="L pr2 to pr3")
prc_abcd.add_mirror(roc_pr3, name="pr3")
prc_abcd.add_space(ll_pr3_bsar, name="L pr3 to bs hr")
prc_abcd.add_mirror(roc_bs, name="bs hr")

prc_abcd.add_space(ll_bs_itmy_ar, name="L bs hr to itmy ar")
prc_abcd.add_lens(f_itmy, name="itmy AR")
prc_abcd.add_space(ll_itmysub, index_of_refraction=nn, name="itmy substrate")
 # reflection from inside itmy changes effective roc
prc_abcd.add_mirror(roc_itmy, index_of_refraction=nn, name="itmy hr") 

prc_abcd.calculate_cavity_abcd()

total_gouy = prc_abcd.get_total_cavity_gouy_phase()
print()
print("PRC ABCD = \n{}\n".format(prc_abcd.abcd))
print()
print("Input Parameters")
print("q = {}".format(prc_abcd.q_input))
print("w = {}".format(prc_abcd.w_input))
print()
print("Accum Gouy Phase = {:.2f} deg".format(total_gouy))

prc_abcd.print()


fig = prc_abcd.plot_cavity_scan(round_trip=False, label="PRC Mode")

# Change input beam so it doesn't match the fundamental mode
# prc_abcd.q_input = (1834.203+427.841j) # estimated q_input from arm cavity (Example 2)
# fig = prc_abcd.plot_cavity_scan(round_trip=False, color='C7', ls='--', label='Mode from Arm Cavity', fig=fig)

s1, s2 = fig.get_axes()

s1.set_title(f"Single-pass PRC beam profile - Gouy = {total_gouy/2:.1f} degs")
s1.set_yticks(np.linspace(-60, 60, 13))
s2.set_yticks(np.linspace(0, 25, 6))

s2.get_legend().remove()

fig.set_size_inches(12, 9)

plt.tight_layout()
plt.savefig(f"{fig_dir}/prc_beam_trace.pdf")
plt.show()
