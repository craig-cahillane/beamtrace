'''
arm_cavity.py

Create a LIGO arm cavity with the same geometric parameters.

R1 = 1934 m        R2 = 2245 m
______              ______
|    /______________\    |
|   /    L=3995 m    \   |
|   \ ______________ /   |
|____\              /____|
'''

import beamtrace
# LIGO arm cavity
import numpy as np
from beamtrace.tracer import BeamTrace

R1 = 1934. # m
R2 = 2245. # m
L = 3994.469 # m

arm_cav = BeamTrace()

arm_cav.add_mirror(R1, name='ITMY')
arm_cav.add_space(L)
arm_cav.add_mirror(R2, name='ETMY')

import matplotlib.pyplot as plt

fig = arm_cav.plot_cavity_scan(round_trip=False)
print(arm_cav.q)
plt.show()
