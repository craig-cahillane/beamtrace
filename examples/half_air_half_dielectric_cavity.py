'''
half_air_half_dielectric_cavity.py
Create a simple two mirror cavity with radius of curvature of 1.0 meter,
and distance between the mirrors of 0.5 meters
R1 = 0.5 m        R2 = 0.5 m
______              ______________
|    /______________|_____________\
|   /    L=50 cm    |    L=50 cm   \
|   \ ______________|_____________ /
|____\              |_____________/
'''

import beamtrace
import numpy as np
from beamtrace.tracer import BeamTrace

length = 0.75 # m
roc1 = 1 # m
roc2 = 1 # m
nn = 1.4585 

my_cav = BeamTrace()  # initializes BeamTrace class
my_cav.add_mirror(roc1, name="Mirror 1")  # adds mirror with 1.0 meter radius of curvature at z=0.0 meters
my_cav.add_space(length, name="Air")
my_cav.add_refraction(np.inf, 1.0, nn, name="Substrate Front Surface") # Only for plotting purposes, does not affect the q-parameter
my_cav.add_space(length, index_of_refraction=nn, name="Substrate")
my_cav.add_mirror(roc2, index_of_refraction=nn, name="Substrate Curved Back Surface") 

my_cav.calculate_cavity_abcd()  # Finds the cavity round-trip ABCD matrix, tells you if it's stable.  If it is stable, populates the my_cav.q_input parameter
zz, ww, gouy, qq = my_cav.scan_cavity(round_trip=True)  

import matplotlib.pyplot as plt

fig = my_cav.plot_cavity_scan(round_trip=False, label="Half Air Half Dielectric Cavity")
plt.show()
