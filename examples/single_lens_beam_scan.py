"""
Example 0
Scan a simple seeded beam scan,
"""

# LIGO arm cavity
import numpy as np
from beamtrace.tracer import BeamTrace

q_input = -1.0 + 1.0j
length1 = 5 # m
lens_focal_length = 1.0 # m
length2 = 3 # m
beam = BeamTrace()

beam.set_seed_beam(q_input)
beam.add_space(length1)
beam.add_lens(lens_focal_length, name="1m Lens")
beam.add_space(length2)

print("Calculate beam scan ABCD")
beam.calculate_beam_scan_abcd()
print(beam.abcd)

import matplotlib.pyplot as plt

fig = beam.plot_beam_scan()
plt.show()
