'''
Example 2
Create a simple two mirror cavity with radius of curvature of 1.0 meter,
and distance between the mirrors of 0.5 meters
R1 = 1934 m        R2 = 2245 m
______              ______
|    /______________\    |
|   /    L=50 cm    \   |
|   \ ______________ /   |
|____\              /____|
'''

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from beamtrace.tracer import BeamTrace

fontsize = 14
mpl.rcParams.update(
    {
        "text.usetex": True,
        "figure.figsize": (12, 9),
        "font.family": "serif",
        "font.serif": "georgia",
        # 'mathtext.fontset': 'cm',
        "lines.linewidth": 2,
        "font.size": fontsize,
        "xtick.labelsize": fontsize,
        "ytick.labelsize": fontsize,
        "legend.fancybox": True,
        "legend.fontsize": fontsize,
        "legend.framealpha": 0.7,
        "legend.handletextpad": 0.5,
        "legend.labelspacing": 0.2,
        "legend.loc": "best",
        "savefig.dpi": 80,
        "pdf.compression": 9,
    }
)

L = 30 # m

data_dict = {}
R1s = np.array([1000, 100]) # m
for R1 in R1s:
    print(f"R1 = {R1}")

    R2 = R1

    fixed_spacer_cav = BeamTrace(is_single_pass=True)

    fixed_spacer_cav.add_mirror(R1, name='input')
    fixed_spacer_cav.add_space(L)
    fixed_spacer_cav.add_mirror(R2, name='output')
    fixed_spacer_cav.add_space(L)
    fixed_spacer_cav.add_mirror(np.inf, name='flat 1')
    fixed_spacer_cav.add_space(L)
    fixed_spacer_cav.add_mirror(np.inf, name='flat 2')
    fixed_spacer_cav.add_space(L)

    fixed_spacer_cav.scan_cavity(round_trip=False)

    data_dict[R1] = {
        "beam_axis": fixed_spacer_cav.zz,
        "beam_radius": fixed_spacer_cav.ww,
    }



fig, s1 = plt.subplots(1)

for R1 in R1s:
    zz = data_dict[R1]["beam_axis"]
    ww = data_dict[R1]["beam_radius"]

    beam_waist = np.min(ww)
    beam_spot_on_mirrors = np.max(ww)

    p1, = s1.plot(
        zz, 
        ww*1e3, 
        label=f"\nMirror RoC = {R1*1e2:.0f} cm\nBeam waist = {beam_waist*1e3:.2f} mm\nBeam spot on mirrors = {beam_spot_on_mirrors*1e3:.2f} mm\n"
    )
    s1.plot(
        zz, 
        -ww*1e3, 
        color = p1.get_color(),
    )

s1.axvline(x=0, color="#4ba6ff", label="Input mirror")
s1.axvline(x=L, color="#4b56ff", ls='--', label="End mirror")
s1.axvline(x=2*L, color="#2b00bb", ls='--', label="Flat 1")
s1.axvline(x=3*L, color="#2b00bb", ls='--', label="Flat 2")
s1.axvline(x=4*L, color="#4ba6ff", label="Input mirror (again)")

s1.grid()
s1.set_xlabel("Beam axis [m]")
s1.set_ylabel("Beam radius [mm]")
s1.set_title(f"Four mirror cavity Gaussian beam profile for L = {L:.0f} m")

s1.legend(bbox_to_anchor=(1.01, 1.01))

plt.tight_layout()
plt.savefig("four_mirror_high_power_cavity.pdf")
