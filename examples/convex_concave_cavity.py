'''
Example 2
Create a simple two mirror cavity with radius of curvature of 1.0 meter,
and distance between the mirrors of 0.5 meters
R1 = 1934 m        R2 = 2245 m
_____               ______
|    \______________\    |
|     |   L=50 cm    \   |
|     |______________/   |
|____/              /____|
'''

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from beamtrace.tracer import BeamTrace

fontsize = 14
mpl.rcParams.update(
    {
        "text.usetex": True,
        "figure.figsize": (12, 9),
        "font.family": "serif",
        "font.serif": "georgia",
        # 'mathtext.fontset': 'cm',
        "lines.linewidth": 2,
        "font.size": fontsize,
        "xtick.labelsize": fontsize,
        "ytick.labelsize": fontsize,
        "legend.fancybox": True,
        "legend.fontsize": fontsize,
        "legend.framealpha": 0.7,
        "legend.handletextpad": 0.5,
        "legend.labelspacing": 0.2,
        "legend.loc": "best",
        "savefig.dpi": 80,
        "pdf.compression": 9,
    }
)

L = 50e-2 # m

data_dict = {}
R1s = np.array([-11, -10, -9]) # m
R2s = np.array([11, 10, 9])
for R1 in R1s:
    data_dict[R1] = {}

    for R2 in R2s:

        print(f"R1 = {R1}")
        print(f"R2 = {R2}")

        g1 = 1 - L / R1
        g2 = 1 - L / R2
        gg = g1 * g2

        print()
        print(f"g1 = {g1}")
        print(f"g2 = {g2}")
        print(f"total g = {gg}")

        fixed_spacer_cav = BeamTrace()

        fixed_spacer_cav.add_mirror(R1, name='input')
        fixed_spacer_cav.add_space(L)
        fixed_spacer_cav.add_mirror(R2, name='output')

        try:
            fixed_spacer_cav.scan_cavity()
        except AttributeError:
            print()
            print("Skipping this cavity")
            print()
            continue

        data_dict[R1][R2] = {
            "beam_axis": fixed_spacer_cav.zz,
            "beam_radius": fixed_spacer_cav.ww,
            "gg": gg,
        }



fig, s1 = plt.subplots(1)

for R1 in R1s:
    for R2 in R2s:
        try:
            zz = data_dict[R1][R2]["beam_axis"]
            ww = data_dict[R1][R2]["beam_radius"]
            gg = data_dict[R1][R2]["gg"]
        except KeyError:
            print("Cavity was skipped, do not plot")
            continue

        beam_spot_on_input_mirror = np.min(ww)
        beam_spot_on_end_mirror = np.max(ww)

        p1, = s1.plot(
            zz, 
            ww*1e3, 
            label=f"\nMirror RoC = {R1:.0f} m\nMirror RoC = {R2:.0f} m\ng1 * g2 = {gg:.4f}\nBeam waist = {beam_spot_on_input_mirror*1e3:.2f} mm\nBeam spot on mirrors = {beam_spot_on_end_mirror*1e3:.2f} mm"
        )
        s1.plot(
            zz, 
            -ww*1e3, 
            color = p1.get_color(),
        )

s1.axvline(x=0, color="#4ba6ff", label="Input mirror")
s1.axvline(x=L, color="#4b56ff", label="End mirror")

s1.grid()
s1.set_xlabel("Beam axis [m]")
s1.set_ylabel("Beam radius [mm]")
s1.set_title(f"Fabry-Perot Gaussian beam profile for fixed cavity L = {L*1e2:.0f} cm")

s1.legend(bbox_to_anchor=(1.01, 1.01))

plt.tight_layout()
plt.savefig("convex_concave_cavity.pdf")
