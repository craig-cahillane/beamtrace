"""
Example 1
Scan a simple seeded beam scan through two flat dielectric surfaces,

            |        \
<--- n1 --->|   n2    |
            |        /

Craig Cahillane
July 14, 2022
"""

import numpy as np
from beamtrace.tracer import BeamTrace

q_input = -1.0 + 1.0j
length = 0.5  # m

roc = 0.5
n1 = 1.0
nn = 2.0

beam = BeamTrace(is_cavity=False)

beam.set_seed_beam(q_input)

# beam.add_space(length, name="normal length")
beam.add_mirror(roc, name="normal curved mirror surface")
beam.add_space(length, name="normal length 2")

beam.print()

# Make an equivalent beam trace without using lens or index of refraction in the space
# Here, the refractions are also just identity matrices, same as the lenses above.
beam2 = BeamTrace(is_cavity=False)

beam2.set_seed_beam(q_input)

# beam2.add_space(length, index_of_refraction=nn, name="substrate length 1")
beam2.add_mirror(roc, index_of_refraction=nn, name="mirror curved back surface")
beam2.add_space(length, index_of_refraction=nn, name="substrate length 2")

beam2.print()

import matplotlib.pyplot as plt

fig = beam.plot_beam_scan(label="In-air reflection")
beam2.plot_beam_scan(color="C4", ls="--", label="Substrate reflection", fig=fig)
axes = fig.get_axes()
axes[0].set_title(f"Beam trace reflection off normal mirror vs back of curved dielectric with index of refraction nn = {nn}")
plt.show()
