"""
Example 0
Scan a simple seeded beam scan,
"""

# LIGO arm cavity
import numpy as np
from beamtrace.tracer import BeamTrace

q_input = -1.0 + 1.0j
length = 5  # m
beam = BeamTrace(is_cavity=False)

beam.set_seed_beam(q_input)
beam.add_space(length)

print("Calculate the ABCD matrix")
beam.calculate_beam_scan_abcd()
print(beam.abcd)

import matplotlib.pyplot as plt

fig = beam.plot_beam_scan()
plt.show()
