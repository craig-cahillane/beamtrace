"""imc_to_prm_beam_scan.py

Beam scan of the path from the input mode cleaner to the power recycling mirror.
Starts at the MC3 high-reflection side and propagates to the PRM high-reflection side.

Code by Kelia Carbin, STAR research fellow,
July 15, 2022
"""
import beamtrace
import matplotlib.pyplot as plt
import numpy as np
from beamtrace.tracer import BeamTrace

print("\n\n==> Path from MC3 through IMs1-4 onto PRM.")

# PRM Propagation Path
nn = 1.44963 # index of refraction of Silica

R_MC3 = np.inf
R_IM1 = np.inf  # m
R_IM2 = 12.8  # m
R_IM3 = -6.24  # m
R_IM4 = np.inf  # m
R_PRM = -11.0  # m

# distances between mirrors
L_M3H_M3A = 0.08204  # m			# MC3 High Refl to MC3 Anti Refl (thick) 
L_M3A_I1 = 0.42822  # m
L_I1_I2 = 1.29373  # m
L_I2_I3 = 1.16952  # m				# CONSISTENT with SC_REFL_HAM1_HAM2
L_I3_I4 = 1.17538  # m				# CONSISTENT with SC_REFL_HAM1_HAM2
L_I4_PRA = 0.41351  # m				# CONSISTENT with SC_REFL_HAM1_HAM2
L_PRA_PRH = 0.07370  # m			# PRM Anti Refl to PRM High Refl (thick)

# initialize class : is single path propagation
IMCPR_II = BeamTrace(is_single_pass=True, is_cavity=False)

# initialize q-input from the IMC eigenmode calculated elsewhere
q_input = -0.23268615302868922+13.339459354452865j
IMCPR_II.set_seed_beam(q_input)

# add mirrors and spaces
IMCPR_II.add_mirror(R_MC3, name="MC3 HR")
IMCPR_II.add_space(L_M3H_M3A, index_of_refraction=nn)
IMCPR_II.add_mirror(np.inf, name="MC3 AR")
IMCPR_II.add_space(L_M3A_I1)
IMCPR_II.add_mirror(R_IM1, name="IM1")
IMCPR_II.add_space(L_I1_I2)
IMCPR_II.add_mirror(R_IM2, name="IM2")
IMCPR_II.add_space(L_I2_I3)
IMCPR_II.add_mirror(R_IM3, name="IM3")
IMCPR_II.add_space(L_I3_I4)
IMCPR_II.add_mirror(R_IM4, name="IM4")
IMCPR_II.add_space(L_I4_PRA)
IMCPR_II.add_mirror(np.inf, name="PRM AR")
IMCPR_II.add_space(L_PRA_PRH, index_of_refraction=nn)
IMCPR_II.add_mirror(R_PRM, name="PRM HR")

IMCPR_II.print()

fig = IMCPR_II.plot_beam_scan()
plt.show()