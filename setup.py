import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="beamtrace",
    version="0.0.15",
    author="Craig Cahillane",
    author_email="craigcahillane@fastmail.com",
    description="beamtrace for quick ABCD matrix calculations",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.ligo.org/craig-cahillane/beamtrace",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
)
